create temporary table products_below_avg
select productid, productname, price from products where price < (select avg(price) from products) limit 0;
 
show table status; 
 
 
drop table products_below_avg;
 
select * from products_below_avg;