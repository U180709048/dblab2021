CREATE DEFINER=`root`@`localhost` PROCEDURE `getCustomersByCountry`(IN c varchar(45))
BEGIN
	select * 
    from customers
    where country = c;
    
    set c = "Turkey";
END